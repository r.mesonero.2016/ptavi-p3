#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):
    def __init__(self):
        # diccionarios
        self.root_layout = {}
        self.region = {}
        self.imagen = {}
        self.audio = {}
        self.textstream = {}
        # lista
        self.lista = []

    def startElement(self, nombre, atributo):
        if nombre == "root-layout":
            self.root_layout["width"] = atributo.get("width", "")
            self.root_layout["height"] = atributo.get("height", "")
            self.root_layout["background-color"] = \
                atributo.get("background-color", "")
            self.lista.append([nombre, self.root_layout])
            self.root_layout = {}
        elif nombre == "region":
            self.region["id"] = atributo.get("id", "")
            self.region["top"] = atributo.get("top", "")
            self.region["bottom"] = atributo.get("bottom", "")
            self.region["left"] = atributo.get("left", "")
            self.region["right"] = atributo.get("right", "")
            self.lista.append([nombre, self.region])
            self.region = {}
        elif nombre == "img":
            self.imagen["src"] = atributo.get("src", "")
            self.imagen["region"] = atributo.get('region', "")
            self.imagen["begin"] = atributo.get("begin", "")
            self.imagen["dur"] = atributo.get("dur", "")
            self.lista.append([nombre, self.imagen])
            self.imagen = {}
        elif nombre == "audio":
            self.audio["src"] = atributo.get("src", "")
            self.audio["begin"] = atributo.get("begin", "")
            self.audio["dur"] = atributo.get("dur", "")
            self.lista.append([nombre, self.audio])
            self.audio = {}
        elif nombre == "textstream":
            self.textstream["src"] = atributo.get("src", "")
            self.textstream["region"] = atributo.get("region", "")
            self.lista.append([nombre, self.textstream])
            self.textstream = {}

    def get_tags(self):
        return self.lista

if __name__ == "__main__":
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    print(cHandler.get_tags())
