#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import urllib.request
import json
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from smallsmilhandler import SmallSMILHandler
import smallsmilhandler


class KaraokeLocal:

    def __init__(self, file):
        parser = make_parser()
        cHandler = SmallSMILHandler()
        parser.setContentHandler(cHandler)
        parser.parse(open(file))
        self.lista = cHandler.get_tags()

    def __str__(self):
        result = ""
        for datos in self.lista:
            tipo_dato = datos[0]
            etiqueta = datos[1]
            result = result + '\n' + tipo_dato + '\t'
            for valor in etiqueta:
                if etiqueta[valor] != '':
                    result = result + valor + '="' + \
                        etiqueta[valor] + '"' + '\t'
        return (result)

    def do_json(self, filesmil, filejson=''):
        if filejson == '':
            filejson = filesmil.replace('.smil', '.json')
        with open(filejson, 'w') as jsonfile:
            json.dump(self.lista, jsonfile, indent=3)

    def do_local(self):
        for datos in self.lista:
            etiqueta = datos[1]
            for atributo in etiqueta:
                if atributo == 'src':
                    if etiqueta[atributo].startswith('http://'):
                        url = etiqueta[atributo]
                        archivo = url.split('/')[-1]
                        urllib.request.urlretrieve(url, archivo)
                        etiqueta[atributo] = archivo

if __name__ == "__main__":

    try:
        file = sys.argv[1]
    except IndexError:
        sys.exit('Usage python3 karaoke.p file.smil')

    listado_ordenada = KaraokeLocal(file)
    print(listado_ordenada)
    listado_ordenada.do_json(file)
    listado_ordenada.do_local()
    listado_ordenada.do_json(file, "local.json")
    print(listado_ordenada)
